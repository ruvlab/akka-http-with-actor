import akka.http.scaladsl.Http
import scala.util.{Failure, Success}

object HttpServer extends App with ActorComponent with RoutingComponent {
  Http().newServerAt("localhost", 8080)
    .bind(route)
    .onComplete {
      case Success(binding) => binding.localAddress
      case Failure(_) => system.terminate()
    }
}
