import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.{Directives, Route}

trait RoutingComponent {
  val route: Route = path("hello") {
        complete(StatusCodes.OK, "That is OK !!!!")
    }
}
